<?php

/** The Model is the class holding data related to one book.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Skier {
	public $username;
	public $fname;
	public $lname;
	public $birthyear;

/** Constructor
 * @param string $username Skier's username
 * @param string $fname Skier's first name
 * @param string $lname Skier's last name
 * @param integer $birthyear Skier's year of birth
 */
	public function __construct($username, $fname, $lname, $birthyear)
    {
        $this->username = $username;
        $this->fname = $fname;
	    $this->lname = $lname;
	    $this->birthyear = $birthyear;
    }
}

?>
