<?php
require_once('etc/config.php');

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;
    public $conn; // connected bool

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
	    else
	    {
			$dsn = 'mysql:host='.Config::HOST.';dbname='.Config::DBNAME.';charset=utf8mb4';
			try {
			  $this->db = new PDO($dsn, Config::DBUSER, Config::DBPASS);
			  $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $ex) {
			  error_log($ex->getMessage());
			}
			$this->conn = ($this->db != NULL);
		}
    }

    public function AddSeason($fallYear) {
        try {
            $query = $this->db->prepare("INSERT IGNORE INTO season (fallYear)
              VALUES (:fY)");
            $query->bindValue(':fY', $fallYear, PDO::PARAM_INT);
            $query->execute();
            return true;
          } catch (PDOException $ex) {
            error_log($ex->getMessage());
            return false;
          }
    }

    public function AddSkier($skier) {
        try {
            $query = $this->db->prepare("INSERT IGNORE INTO skier
              VALUES (:un, :fn, :ln, :by)");
            $query->bindValue(':un', $skier->username, PDO::PARAM_STR);
            $query->bindValue(':fn', $skier->fname, PDO::PARAM_STR);
            $query->bindValue(':ln', $skier->lname, PDO::PARAM_STR);
            $query->bindValue(':by', $skier->birthyear, PDO::PARAM_INT);
            $query->execute();
            return true;
          } catch (PDOException $ex) {
            error_log($ex->getMessage());
            return false;
          }
    }

    public function AddClub($club) {
        try {
            $query = $this->db->prepare("INSERT IGNORE INTO club
              VALUES (:ci, :cn, :cc, :cf)");
            $query->bindValue(':ci', $club->clubId, PDO::PARAM_STR);
            $query->bindValue(':cn', $club->name, PDO::PARAM_STR);
            $query->bindValue(':cc', $club->city, PDO::PARAM_STR);
            $query->bindValue(':cf', $club->county, PDO::PARAM_STR);
            $query->execute();
            return true;
          } catch (PDOException $ex) {
            error_log($ex->getMessage());
            return false;
          }
    }

    public function AddParticipation($username, $fallYear, $clubId, $totalDist) {
        try {
            $query = $this->db->prepare("INSERT IGNORE INTO participation
              VALUES (:un, :fy, :ci, :td)");
            $query->bindValue(':un', $username, PDO::PARAM_STR);
            $query->bindValue(':fy', $fallYear, PDO::PARAM_INT);
            $query->bindValue(':ci', $clubId, PDO::PARAM_STR);
            $query->bindValue(':td', $totalDist, PDO::PARAM_INT);
            $query->execute();
            return true;
          } catch (PDOException $ex) {
            error_log($ex->getMessage());
            return false;
          }
    }

    public function AddLogEntry($username, $fallYear, $entry) {
        try {
            $query = $this->db->prepare("INSERT IGNORE INTO log_entries
              VALUES (:un, :fy, :edt, :ea, :eds)");
            $query->bindValue(':un', $username, PDO::PARAM_STR);
            $query->bindValue(':fy', $fallYear, PDO::PARAM_INT);
            $query->bindValue(':edt', $entry->date, PDO::PARAM_STR);
            $query->bindValue(':ea', $entry->area, PDO::PARAM_STR);
            $query->bindValue(':eds', $entry->distance, PDO::PARAM_INT);
            $query->execute();
            return true;
          } catch (PDOException $ex) {
            error_log($ex->getMessage());
            return false;
          }
    }
}

?>
