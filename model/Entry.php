<?php

/** The Model is the class holding data related to one book.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Entry {
	public $date;
	public $area;
	public $distance;

/** Constructor
 * @param string $date Entry log's date
 * @param string $area Area skied in
 * @param string $distance The distance the skier traveled
 */
	public function __construct($date, $area, $distance)
    {
        $this->date = $date;
        $this->area = $area;
	    $this->distance = $distance;
    }
}

?>
