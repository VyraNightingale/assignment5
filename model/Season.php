<?php

/** The Model is the class holding data related to one book.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Season {
	public $fallYear;

/** Constructor
 * @param integer $fallYear The Fall/Winter Season of the year
 */
	public function __construct($fallYear)
    {
        $this->fallYear = $fallYear;
    }
}

?>
