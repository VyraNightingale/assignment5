<?php

/** The Model is the class holding data related to one book.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Club {
	public $clubId;
	public $name;
	public $city;
	public $county;

/** Constructor
 * @param string $clubId Club's id
 * @param string $name Name of the club
 * @param string $city The city the club is from
 * @param string $county The county the club is from
 */
	public function __construct($clubId, $name, $city, $county)
    {
        $this->clubId = $clubId;
        $this->name = $name;
	    $this->city = $city;
	    $this->county = $county;
    }
}

?>
