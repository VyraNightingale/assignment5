<?php
include_once('View.php');

/** The BookView is the class that creates the page showing details about one book.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
Class UploadView extends View {

    /** Constructor
     * @author Rune Hjelsvold
     * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
     */

	 protected $message = null;
	 /** Set a message to be given to the end user if
	  * @$msg string The message to pass to the user.
	  */
	 public function __construct($msg = null)
	 {
	 	if ($msg) {
	 		$this->message = $msg;
		}
	 }

	/** Used by the superclass to generate page title
	  * @return string Page title.
	  */
	protected function getPageTitle() {
		return 'Upload XML';
	}

	/** Helper function generating HTML code for the form for uploading XML to be parsed
	 */
	protected function createUploadButton() {
		return '<h4>'.$this->message.'</h4>'
		. '<form id="upForm" action="index.php" method="post" enctype="multipart/form-data">'
		. '<input name="xml_file" id="xml_file" type="file" />'
        . '<input type="submit" value="Upload XML" name="submit" />'
        . '</form>';
	}

	/** Used by the superclass to generate page content
	 */
	protected function getPageContent() {
        return $this->createUploadButton();
	}
}
?>
