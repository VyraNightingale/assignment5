<?php
require_once("model/DBModel.php");
include_once("model/Skier.php");
include_once("model/Club.php");
include_once("model/Season.php");
include_once("model/Entry.php");
include_once("view/UploadView.php");
include_once("view/ErrorView.php");

/** The Controller is responsible for handling user requests, for exchanging data with the Model,
 * and for passing user response data to the various Views.
 * @author Rune Hjelsvold
 * @see model/Model.php The Model class holding book data.
 * @see view/viewbook.php The View class displaying information about one book.
 * @see view/booklist.php The View class displaying information about all books.
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Controller {
	public $model;

	public function __construct()
   {
		session_start();
		$this->model = new DBModel();
		if (!$this->model->conn) // No user connected to DB
		{
			$view = new ErrorView("No DB connection");
			$view->create();
			die();
		}
    }

/** The one function running the controller code.
 */
	public function invoke()
	{
		if(isset($_POST["submit"])) {
			// Load XML into DOM and feed to model
			$doc = new DOMDocument();
			if (!$doc->load($_FILES["xml_file"]["tmp_name"])) {
				$view = new ErrorView("Couldn't fetch XML file");
				$view->create();
				die;
			}
			$x = new DOMXPath($doc);
			$root = $x->query('./SkierLogs')->item(0);

			// Seasons, Logs & Participation
			$seasons = $x->query('./Season', $root);
			foreach ($seasons as $season) {
				$fallYear = $season->getAttribute('fallYear');
				if (!$this->model->AddSeason($fallYear)) {
					$view = new ErrorView("Couldn't insert season into DB");
					$view->create();
					die;
				}

				$seasonClubs = $x->query('./Skiers', $season);
				foreach ($seasonClubs as $seasonClub) {
					$clubId = null;
					if ($seasonClub->hasAttribute('clubId')) {
						$clubId = $seasonClub->getAttribute('clubId');
					}

					$seasonSkiers = $x->query('./Skier', $seasonClub);
					foreach ($seasonSkiers as $seasonSkier) {
						$username = $seasonSkier->getAttribute('userName');
						$sum = 0;

						$entries = $x->query('./Log/Entry', $seasonSkier);
						foreach ($entries as $entry) {
							$dist = $x->query('./Distance', $entry)->item(0)->nodeValue;
							$area = $x->query('./Area', $entry)->item(0)->nodeValue;
							$date = $x->query('./Date', $entry)->item(0)->nodeValue;
							$entryObj = new Entry($date, $area, $dist);

							if (!$this->model->AddLogEntry($username, $fallYear, $entryObj)) {
								$view = new ErrorView("Couldn't insert log entry into DB");
								$view->create();
								die;
							}

							$sum += $dist;
						}

						if (!$this->model->AddParticipation($username, $fallYear, $clubId, $sum)) {
							$view = new ErrorView("Couldn't insert participation details into DB");
							$view->create();
							die;
						}
					}
				}
			}

			// Skiers
			$skiers = $x->query('./Skiers/Skier', $root);
			foreach ($skiers as $skier) {
				$fname = $x->query('./FirstName', $skier)->item(0)->nodeValue;
				$lname = $x->query('./LastName', $skier)->item(0)->nodeValue;
				$birthyear = $x->query('./YearOfBirth', $skier)->item(0)->nodeValue;
				$username = $skier->getAttribute('userName');
				$skierObj = new Skier($username, $fname, $lname, $birthyear);
				if (!$this->model->AddSkier($skierObj)) {
					$view = new ErrorView("Couldn't insert skier into DB");
					$view->create();
					die;
				}
			}

			// Clubs
			$clubs = $x->query('./Clubs/Club', $root);
			foreach ($clubs as $club) {
				$name = $x->query('./Name', $club)->item(0)->nodeValue;
				$city = $x->query('./City', $club)->item(0)->nodeValue;
				$county = $x->query('./County', $club)->item(0)->nodeValue;
				$clubId = $club->getAttribute('id');
				$clubObj = new Club($clubId, $name, $city, $county);
				if (!$this->model->AddClub($clubObj)) {
					$view = new ErrorView("Couldn't insert club into DB");
					$view->create();
					die;
				}

			}

			$view = new UploadView("Submitted");
			$view->create();
		} else {
			$view = new UploadView();
			$view->create();
		}
	}
}

?>
